module.exports = {
    plugins: [
        require('prettier-plugin-tailwindcss'),
      ],
      pluginSearchDirs: false,
      singleQuote: true,
      tailwindConfig: './tailwind.config.cjs',
};
