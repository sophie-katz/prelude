module.exports = {
  plugins: {
    'postcss-import': {},
    tailwindcss: {
      config: './tailwind.config.cjs',
    },
    autoprefixer: {},
    'postcss-extend-rule': {},
    'postcss-simple-vars': {},
    'postcss-for': {},
  },
};
