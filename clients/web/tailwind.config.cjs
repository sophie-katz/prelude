/** @type {import('tailwindcss').Config} */

const shades = [50, 100, 200, 300, 400, 500, 600, 700, 800, 900];

function generateColor(baseHue, baseSaturation) {
  let result = {};

  for (const shade of shades) {
    result[shade] =
      'hsl(' +
      baseHue +
      ', ' +
      baseSaturation * 100 +
      '%, ' +
      (1000 - shade) / 10 +
      '%)';
  }

  return result;
}

module.exports = {
  content: ['./index.html', './src/**/*.vue|ts|css'],
  theme: {
    fontFamily: {
      sans: ['Poppins', 'sans-serif'],
      mono: ['Menlo', 'Monaco', 'Consolas', 'monospace'],
    },
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
      white: '#ffffff',
      black: '#000000',
      primary: '#36ACFB',
      'primary-variant': '#168CDB',
      secondary: '#81D176',
      'secondary-variant': '#51A146',
      tertiary: '#F47340',
      'tertiary-variant': '#D45320',
      gray: generateColor(250, 0.05),
      red: generateColor(0, 0.9),
      orange: generateColor(20, 0.9),
      yellow: generateColor(40, 0.98),
      lime: generateColor(90, 0.9),
      green: generateColor(130, 0.8),
      teal: generateColor(170, 0.8),
      cyan: generateColor(190, 0.8),
      blue: generateColor(230, 0.8),
      purple: generateColor(290, 0.7),
      pink: generateColor(340, 0.8),
    },
    extend: {},
  },
  plugins: [],
};
