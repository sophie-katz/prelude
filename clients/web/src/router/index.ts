import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';

const routes: Array<RouteRecordRaw> = [
  {
    path: '/demo/typescale',
    name: 'Typescale Demo',
    component: () => import('../components/Demo/Typescale/TypescaleDemo.vue'),
  },
  {
    path: '/demo/color-system',
    name: 'Color System Demo',
    component: () =>
      import('../components/Demo/ColorSystem/ColorSystemDemo.vue'),
  },
  {
    path: '/demo/spacings',
    name: 'Spacings Demo',
    component: () => import('../components/Demo/Spacings/SpacingsDemo.vue'),
  },
];

export default createRouter({
  history: createWebHistory(),
  routes,
});
