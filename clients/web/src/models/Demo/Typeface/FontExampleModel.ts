export default class FontExampleModel {
  public constructor(public text: string, public classes: Array<string>) {}
}
