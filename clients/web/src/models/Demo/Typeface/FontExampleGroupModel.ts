import FontExampleModel from './FontExampleModel';

export default class FontExampleGroupModel {
  public constructor(
    public name: string,
    public examples: Array<FontExampleModel>
  ) {}
}
