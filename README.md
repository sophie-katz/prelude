<!--
MIT License

Copyright (c) 2023 Sophie Katz

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
-->

# Prelude

<img src="https://img.shields.io/badge/license-MIT-green" /> <img src="https://img.shields.io/badge/rust-1.69.0-blue" /> <img src="https://img.shields.io/badge/node-v18.13.0-blue" /> <img src="https://img.shields.io/badge/platform-linux%7Cdocker-lightgrey" />

Prelude is a project management, integration, and monitoring suite.

## Guides

- See [Installation](https://sophie-katz-prelude.readthedocs.io/en/latest/installation.html) for how to install and set up Prelude.
- See [Contributing](https://sophie-katz-prelude.readthedocs.io/en/latest/contributing.html) for details on how to set this project up for local development.

## Resources

| Link                                                                                                           | Description                                                                      |
| -------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------- |
| [Documentation](https://sophie-katz-prelude.readthedocs.io/en/latest/)                                         | Prelude documentation for developer use                                          |
| [Nuclino Workspace](https://app.nuclino.com/Sophie-Katz/Prelude)                                               | Internal knowledge base                                                          |
| [Asana Team](https://app.asana.com/0/1204222021001389/overview)                                                | Project management                                                               |
| [Google Drive folder](https://drive.google.com/drive/folders/1N00nt2MpcOYI9LJROfeZS94XxQnfkklY?usp=share_link) | Google Drive storage location                                                    |
| [REST API Documentation](https://prelude.stoplight.io/docs/prelude)                                            | Documentation for REST APIs generated from OpenAPI specifications with Spotlight |
| [Figma Project](https://www.figma.com/files/project/85231228/Prelude)                                          | Figma project for UI design                                                      |
