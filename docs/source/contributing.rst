.. MIT License

   Copyright (c) 2023 Sophie Katz

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in all
   copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.

Contributing to Prelude
=======================

Prelude is intended to be developed inside VS Code. Everything that can be done inside VS Code can be done through other editors or via the command line, but these workflows are not supported.

Installing dependencies on your system
--------------------------------------

Development may be done on Windows, macOS, or Linux. The following dependencies need to be installed, however:

* `VS Code <https://code.visualstudio.com/>`_

* `Docker <https://www.docker.com/>`_

  * On Windows or macOS, install Docker Desktop

  * On Linux, install Docker server following `these instructions <https://docs.docker.com/engine/install/>`_.

* `Docker Compose <https://docs.docker.com/compose/install/>`_


.. note::
    Docker Desktop is most likely already installed with Docker. Use the command ``docker-compose --version`` to check (can be run on any supported platform).

Opening the project in VS Code
------------------------------

Follow these steps to get your VS Code development environment up and running.

* **Clone:** Clone the source code from this repo.

* **Open:** Open this directory in VS Code.

* **Enter Dockerized environment:** Click the green button on the bottom left of the VS Code window, and then select "Reopen in Container". See `https://code.visualstudio.com/docs/remote/containers <https://code.visualstudio.com/docs/remote/containers>`_ for more details.

    * It will take a while when done for the first time as it needs to download and build Docker images. Click "Show log" to see progress.

    * Once it completes, VS Code will be running inside a Dockerized development environment. Any code running inside VS Code is guaranteed to behave exactly the same as in production.

    * It will also start up third party services which will run alongside Prelude and allow it to run as in production.

Run ``docker ps`` to see the running containers.

Bootstrapping project
---------------------

Run this command to bootstrap your local development environment:

.. code-block:: shell

    ./monorepo/bootstrap

There are multiple bootstraps for different development needs. Follow the instructions in the command output for how to install more than just the general-purpose use one.
