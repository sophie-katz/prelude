.. Prelude documentation master file, created by
   sphinx-quickstart on Wed Mar 22 19:50:29 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Prelude
=======

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   contributing.rst
   installation.rst
   local_port_map.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
